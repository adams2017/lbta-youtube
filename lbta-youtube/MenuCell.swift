//
//  MenuCell.swift
//  lbta-youtube
//
//  Created by Admin on 4/16/19.
//  Copyright © 2019 AX Software. All rights reserved.
//

//Supplies cells for the MenuBar custom control
//Requires "icon_home" asset or change it to an existing asset

import UIKit

class MenuCell: UICollectionViewCell {
    
    private let isHighlightedOrSelectedColor  = UIColor.white
    private let isNotHighlightedOrSelectedColor = UIColor(red: 90/255, green: 13/255, blue: 13/255, alpha: 1)
    private let iconHeight:CGFloat = 32
    private let iconWidth:CGFloat = 32
    private let defaultIcon = "icon_home"  //must be an asset
    
    //Mark: PROPERTIES
    static var reuseIdentifier: String {
        return self.description()
    }
    //MARK: LIFECYCLE
    override init(frame: CGRect) {
        super .init(frame: frame)
        setup()
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func setup() {
        [iconImageView ].forEach {
            $0.translatesAutoresizingMaskIntoConstraints = false
            self.addSubview($0)
        }
        NSLayoutConstraint.activate([
            iconImageView.heightAnchor.constraint(equalToConstant: iconHeight),
            iconImageView.widthAnchor.constraint(equalToConstant: iconWidth),
            iconImageView.centerXAnchor.constraint(equalTo: self.centerXAnchor),
            iconImageView.centerYAnchor.constraint(equalTo: self.centerYAnchor)
        ])
       
    }
    // A collection view cell property
    override var isHighlighted: Bool {
        didSet {
            iconImageView.tintColor = isHighlighted ? isHighlightedOrSelectedColor : isNotHighlightedOrSelectedColor
        }
    }
    // A collection view cell property
    override var isSelected:  Bool {
        didSet {
            iconImageView.tintColor = isSelected ? isHighlightedOrSelectedColor: isNotHighlightedOrSelectedColor
        }
    }
    lazy var iconImageView:UIImageView = {
        let ui = UIImageView()
        ui.image = UIImage(named: self.defaultIcon )?.withRenderingMode(.alwaysTemplate)
        //ui.tintColor = isNotHighlightedOrSelectedColor
        ui.contentMode = UIView.ContentMode.scaleAspectFill
        ui.clipsToBounds = true
        return ui
    }()
}

