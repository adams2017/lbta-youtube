//
//  ApiService.swift
//  lbta-youtube
//
//  Created by Admin on 4/20/19.
//  Copyright © 2019 AX Software. All rights reserved.
//

import Foundation
class ApiService:NSObject {
    
    static let shared = ApiService()
    
    func fetchFeed(for urlString: String, completion: @escaping ([Video]) -> ()) {
        guard let url = URL(string: urlString) else {
            fatalError("Invalid url string")
        }
        print("parsed api ok!!!!!!!!!")
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            print("processing data task")
            if error != nil {
                print(error ?? "")
                return
            }
            do {
                guard let data = data else {
                    fatalError("No data!")
                }
                let decoder = JSONDecoder()
                decoder.keyDecodingStrategy = .convertFromSnakeCase
                let videos = try decoder.decode([Video].self, from: data)
                print("Calling completion!")
                DispatchQueue.main.async {
                    completion(videos)
                }
            } catch let jsonError {
                print(jsonError)
            }
        }.resume()
    }
}
