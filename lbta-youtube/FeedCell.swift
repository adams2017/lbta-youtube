//
//  FeedCell.swift
//  lbta-youtube
//
//  Created by Admin on 4/23/19.
//  Copyright © 2019 AX Software. All rights reserved.
//

import UIKit

//There are 4 Feed cells in a horizontal collection
//Each one will contain a vertial collection containing multiple cells
//for that particular feed

class FeedCell:UICollectionViewCell,
   UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout
 {
    let cellid = "cellid"
    var videos:[Video] = []
    let endpointString = "https://s3-us-west-2.amazonaws.com/youtubeassets/home.json"

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupViews()
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupViews() {
        fetchVideos()  // populate the videos array 
        
        [collectionView].forEach {
            $0.translatesAutoresizingMaskIntoConstraints = false
            self.addSubview($0)
        }
        
        NSLayoutConstraint.activate([
        collectionView.topAnchor.constraint(equalTo: self.topAnchor, constant: 0),
        collectionView.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 8),
        collectionView.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: 0),
        collectionView.rightAnchor.constraint(equalTo: self.rightAnchor, constant: 0)])
        
        collectionView.register(VideoCell.self, forCellWithReuseIdentifier: VideoCell.reuseIdentifier)
    }
    
    // Collection View Delegate methods
    
         func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
            return videos.count
        }
        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
            var height = (self.frame.width - 32) * 0.5625  // 16:9 ration
            height = height + 16 + 68
            return CGSize(width: self.frame.width, height: height )
        }
         func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: VideoCell.reuseIdentifier, for: indexPath) as! VideoCell
            cell.video = videos[indexPath.item]
            cell.backgroundColor = .green
            return cell
        }
    
        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
            return 0
        }
 
    // =================
    
    func fetchVideos() {
        
        ApiService.shared.fetchFeed(for: endpointString) { (arrOfVideos) in
            print("back from fetch ")
            self.videos =  arrOfVideos
            self.collectionView.reloadData()
        }
    }
    
   // GUI Elements 
    lazy var collectionView:UICollectionView = {
        let flow = UICollectionViewFlowLayout()
        flow.minimumInteritemSpacing = 0
        flow.minimumLineSpacing = 0
        flow.scrollDirection = .vertical
        let ui = UICollectionView(frame: .zero, collectionViewLayout: flow)
        ui.backgroundColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1 )
        ui.isPagingEnabled = true
        ui.dataSource = self
        ui.delegate = self
        return ui
    }()
    
}
