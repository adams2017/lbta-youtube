//
//  Setting.swift
//  lbta-youtube
//
//  Created by Admin on 4/20/19.
//  Copyright © 2019 AX Software. All rights reserved.
//

import Foundation

struct Setting {
    
    let name: SettingType
    let nameOfIcon:String
    
}

enum SettingType:String {
    case account = "Account/Profile"
    case privacy = "Privacy and Terms"
    case cancel = "Cancel & DIsmiss"
}
