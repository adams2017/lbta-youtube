//
//  ViewController.swift
//  lbta-youtube
//
//  Created by Admin on 4/8/19.
//  Copyright © 2019 AX Software. All rights reserved.
//

import UIKit

class HomeViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout {

  //Lifecyle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
        setupMenuBar()
        setupNavBarButtons()

       // ApiService.shared.fetchFeed(for: endpointString, completion: processResult(_:))
 
    }
    
    //Not currently used, but if we didn't want to use a closue we coulc cll this 
//    func processResult(_ arrVideos: Array<Video>) {
//        self.videos = arrVideos
//        self.collectionView.reloadData()
//    }
    
    func setup() {
    
        navigationController?.navigationBar.isTranslucent = false
        
        let titleLabel = UILabel(frame: CGRect(x: 0, y: 0, width: view.frame.width-32, height: view.frame.height))
        titleLabel.textColor = .white
        titleLabel.font = UIFont.systemFont(ofSize: 20)
        
        navigationItem.titleView = titleLabel
        setTitle(for: 0)
        setupCollectionView()
 
    }
    let cellID = "cellID"
    func setupCollectionView() {
        //ep 28
        if  let flow = collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            flow.scrollDirection = .horizontal
            flow.minimumLineSpacing = 0
        }
        collectionView.backgroundColor = .white
        //collectionView.register(VideoCell.self, forCellWithReuseIdentifier: VideoCell.reuseIdentifier)
     //collectionView.register(UICollectionViewCell.self, forCellWithReuseIdentifier: cellID)
        
        collectionView.register(FeedCell.self, forCellWithReuseIdentifier: cellID)
        
        collectionView.contentInset = UIEdgeInsets(top: 50, left: 0, bottom: 0, right: 0)
        collectionView.scrollIndicatorInsets = UIEdgeInsets(top: 50, left: 0, bottom: 0, right: 0)
        collectionView.isPagingEnabled = true
    
    }
    //Menu bar is under the Nav Bar
    func setupMenuBar() {
        //
        //HIDE THE TOP NAV BAR WHEN WE SWIPE UP THE COLLECTION VIEW
        //
        navigationController?.hidesBarsOnSwipe = true
        
        [aMenuBar].forEach{
            $0.translatesAutoresizingMaskIntoConstraints=false
             view.addSubview(aMenuBar)
        }
        NSLayoutConstraint.activate([
            aMenuBar.topAnchor.constraint(equalTo: self.view.topAnchor),
            aMenuBar.leftAnchor.constraint(equalTo: self.view.leftAnchor),
            aMenuBar.rightAnchor.constraint(equalTo: self.view.rightAnchor),
            aMenuBar.heightAnchor.constraint(equalToConstant: 50)
            ])
    }
 
    func setupNavBarButtons() {
        navigationItem.rightBarButtonItems = arrayBarButtonItems
   }
    
    //Methods ADDED for ep 28
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 4
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellID, for: indexPath)
        //ep 28
       // let ix = indexPath.row
       // let colors:Array<UIColor> = [.blue, .green, .gray, .purple ]
        //cell.backgroundColor = colors[ix]
      
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width, height: view.frame.height-50)
    }
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        #warning("What is scrollbar content offset x? 0 to 1242 ")
        print("cv content size",collectionView.contentSize  )
        print("cv frame size", collectionView.frame.size)
        print("Scrollview offset x =", scrollView.contentOffset.x )
        print("Scrollview offset y = ", scrollView.contentOffset.y )
        print("Scrollview  x/4= ", scrollView.contentOffset.x / 4 )
        
        aMenuBar.hBarLeft.constant = scrollView.contentOffset.x / 4
        
    }
    
    //MARK: GUI Elements
    
    //ep 12
    lazy var aMenuBar:MenuBar = {
        let ui = MenuBar()
        ui.homeController = self // menu bar can call selectItemAt...
        return ui
    }()
    
    lazy var arrayBarButtonItems:Array<UIBarButtonItem> = {
        let icon1 = #imageLiteral(resourceName: "icons8-menu_2")
        let bb1 = UIBarButtonItem(image: icon1,
                                 style: .plain,
                                 target: self,
                                 action: #selector(settingsBarButtonTouchSelector))
        bb1.tintColor = .white

        let icon2 = #imageLiteral(resourceName: "icons8-1_filled")
        let bb2 = UIBarButtonItem(image: icon2,
                                  style: .plain,
                                  target: self,
                                  action: #selector(positiveBarButtonTouchSelector))
        bb2.tintColor = .white
        return [bb1,bb2]
    }()
    
    //The settings launcher has the ability to show a view listing the settings menu
    //Since it has a reference to HomeViewController, it will be able to call
    //its showViewController which can push the appropriate VC in the NC stack
    lazy var settingsLauncher:SettingsLauncher = {
        let sl = SettingsLauncher()
        #warning("perhaps make self part of the initializer")

        sl.homeViewController = self
        return sl
    }()
    //Cause the settings menu to appear
    @objc func settingsBarButtonTouchSelector() {
        settingsLauncher.showSettings()
    }
    @objc func positiveBarButtonTouchSelector() {
        print("positive touched , but not yet implemented")
        scrollToMenuIndex(menuIndex: 2)
    }
    
    // ep 12 
    func scrollToMenuIndex(menuIndex:Int) {
        let ip = IndexPath(item: menuIndex, section: 0)
        let ix = ip.item
        setTitle(for: ix)
        collectionView.scrollToItem(at: ip, at: UICollectionView.ScrollPosition.centeredHorizontally, animated: false)
    }
    
    let titles = ["Home", "Trending", "Subscriptions", "Account" ]
    override func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        print(targetContentOffset.pointee.x , view.frame.width )
        let ix = Int(targetContentOffset.pointee.x / view.frame.width)
        // first page, x = 0, second page x = 375.  view frame width is 375, so each page is a 375 point chunk
        
        // since paging is on, ix will always be increment of 375 since it stops at a full page increment
        
        print("PERCENT===================", targetContentOffset.pointee.x/view.frame.width*100)
        
        let ip = NSIndexPath(item: ix, section: 0)
        //makes one of the 4 icons white
          aMenuBar.menuCollectionView.selectItem(at: ip as IndexPath, animated: true, scrollPosition: .centeredHorizontally)
        
         setTitle(for: ix)
    }
    
    func setTitle(for index:Int) {
        if  let titleLabel = navigationItem.titleView as? UILabel  {
            titleLabel.text = String(repeating: ".", count: 4) +  titles[index]
        }
    }
    
    //Put another view controller in the NC stack so it will be shown
    //We can access our NC since it is a property of this view controller
    //It is not a property of the UIView that presents the list of settings choices
    
    func showViewController(for setting:Setting) {
        // switch based on the setting enumeration to
        // display the correct view controller
        switch setting.name {
        case .account:
            print(setting.name)
            navigationController?.pushViewController(AccountViewController(), animated: true)
        case .privacy:
            print(setting.name)
            navigationController?.pushViewController(PrivacyViewController(), animated: true)
        case  _:
            print("Ignoring", setting.name )
        }
    
    }
}

