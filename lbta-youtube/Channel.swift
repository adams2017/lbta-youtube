//
//  Channel.swift
//  lbta-youtube
//
//  Created by Admin on 4/17/19.
//  Copyright © 2019 AX Software. All rights reserved.
//

import Foundation

class Channel : Decodable{
    var name:String?
    var profileImageName:String?
}
