//
//  SettingsLauncher.swift
//  lbta-youtube
//
//  Created by Admin on 4/20/19.
//  Copyright © 2019 AX Software. All rights reserved.
//

import UIKit
// Contains the logic to darken the view, display a view which can be dismissed or touched to show a detial screen
// Usage
// let sl = SettingsLauncher()
// sl.showSettings()
//    will create a UIView and populate it with names of settings options
//    clicking on the choice will call a HomeViewController method to push the appropriabe VC on to the NavCon

//THIS IS NOT A VIEW OR A UIViewController
class SettingsLauncher: NSObject, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    let cellHeight:CGFloat = 50
    
    weak var homeViewController:HomeViewController?
    
    let settings:[Setting] = {
        return [Setting(name: .privacy, nameOfIcon: "icons8-lock_filled"),
                Setting(name: .account, nameOfIcon: "icons8-add_user_filled"),
                Setting(name: .cancel, nameOfIcon: "icons8-cancel_filled")]
    }()
    
    // Initialize the collection view by registering the SettingsCell
    override init() {
        super.init()
        settingsCollectionView.register(SettingsCell.self, forCellWithReuseIdentifier: SettingsCell.reuseIdentifier)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
       return settings.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell  = settingsCollectionView.dequeueReusableCell(withReuseIdentifier: SettingsCell.reuseIdentifier, for: indexPath) as! SettingsCell
        cell.setting = settings[indexPath.item]
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width:  collectionView.frame.width , height: cellHeight)
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let ix = indexPath.item

        UIView.animate(
            withDuration: 0.5,
            delay: 0,
            usingSpringWithDamping: 1,
            initialSpringVelocity: 1,
            options: .curveEaseOut,
            animations: {
                self.blackView.alpha = 0 //make the black view transparent
                if let keyWin = UIApplication.shared.keyWindow {
                    self.settingsCollectionView.frame = CGRect(x: 0,y: keyWin.frame.height, width: keyWin.frame.width, height: keyWin.frame.height)
                }})
            { (bool) in
                //read to push new vc from the Home Controller's NAV Controller,
                //And we will then have ability to Nav back to home controller
                if self.settings[ix].name !=  SettingType.cancel {
                   self.homeViewController?.showViewController(for: self.settings[ix])
                }
            }
        
    }
    
    
    let blackView = UIView()
    
    lazy var settingsCollectionView:UICollectionView = {
        let flow = UICollectionViewFlowLayout()
        flow.minimumInteritemSpacing = 0
        flow.minimumLineSpacing = 0
        flow.scrollDirection = .vertical
        let ui = UICollectionView(frame: .zero, collectionViewLayout: flow)
        ui.backgroundColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1 )
        ui.dataSource = self
        ui.delegate = self
        return ui
    }()

    
    @objc func showSettings()  {
        print("settings touched ")
        let keyWindow = UIApplication.shared.keyWindow!
        //This is the pop-up view. It is black and will be animated from totally transparent to 50% transparent
        blackView.backgroundColor =  UIColor(red: 0, green: 0, blue: 0, alpha: 1) // OR  UIColor(white: 0, alpha: 1)
        blackView.frame = keyWindow.frame
        blackView.alpha = 0 //totally  transparent
        // the black view and collection view are both subviews of  the UIWindow

        keyWindow.addSubview(blackView)  // add the dark view over thw whole window
        //Allow the UIView to respond to touch by attaching a Gesture Recognizer
        blackView.addGestureRecognizer(UITapGestureRecognizer(target: self,
                                                              action: #selector(dismissSelector)))
        
        keyWindow.addSubview(settingsCollectionView) // add the collection view
        //blackView.addSubview(settingsCollectionView)  // if you did this instead, it would be trnaspareent not solid because
        //blackView is transparent
        // set size of collection view
        settingsCollectionView.frame = CGRect(x: 0, y: keyWindow.frame.height, width: keyWindow.frame.width, height: 200)
        UIView.animate(withDuration: 0.5) {
            self.blackView.alpha = 0.5 // or make color .5 and this 1
            self.settingsCollectionView.frame = CGRect(
                x: 0,
                y: keyWindow.frame.height - 200, //This may not work for large # settings
                width: keyWindow.frame.width,
                height: CGFloat(self.settings.count) * self.cellHeight)
        }
    }
    //We are dismissing the settings view by making it transparent, and animating it to a position below the Window
    @objc func dismissSelector() {
        UIView.animate(withDuration: 0.5) {
            self.blackView.alpha = 0 //make the black view transparent
            if let keyWin = UIApplication.shared.keyWindow {
                self.settingsCollectionView.frame = CGRect(
                    x: 0,
                    y: keyWin.frame.height,  //its moved off screen
                    width: keyWin.frame.width,
                    height: keyWin.frame.height)
            }
        }
    }
}
