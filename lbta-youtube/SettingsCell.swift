//
//  SettingsCell.swift
//  lbta-youtube
//
//  Created by Admin on 4/20/19.
//  Copyright © 2019 AX Software. All rights reserved.
//

import UIKit
//The settings menu view shows a list of available settings menus
//Each setting menu is a line in the collection view controller
//An icon is on the left, followed by a label
class SettingsCell: UICollectionViewCell {
    
    //Mark: PROPERTIES
    static var reuseIdentifier: String {
        return self.description()
    }
    //Change the shading/tint of the menu selection when touched
    override var isHighlighted: Bool {
        didSet {
            self.backgroundColor = isHighlighted ? UIColor.darkGray : UIColor.white
            settingLabel.backgroundColor = backgroundColor
            settingLabel.textColor = isHighlighted ? UIColor.white:UIColor.darkGray
            iconImageView.tintColor = isHighlighted ? UIColor.white:UIColor.darkGray
        }
    }
    //MARK: LIFECYCLE
    override init(frame: CGRect) {
        super .init(frame: frame)
        setup()
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    //Inject a new setting value to populate the gui elements
    var setting:Setting? {
        didSet {
            guard let setting = setting  else {return}
            settingLabel.text = setting.name.rawValue
            settingLabel.textColor = UIColor.darkGray
            iconImageView.image = UIImage(named: setting.nameOfIcon)?.withRenderingMode(.alwaysOriginal)
        }
    }
    //Add the GUI elements to the cell
    func setup() {
        [settingLabel,iconImageView].forEach {
            $0.translatesAutoresizingMaskIntoConstraints = false
            self.addSubview($0)
        }
        
        NSLayoutConstraint.activate([
           // iconImageView.topAnchor.constraint(equalTo: self.topAnchor, constant: 0),
            iconImageView.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 8),
           // iconImageView.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: 0),
            iconImageView.widthAnchor.constraint(equalToConstant: 40),
            iconImageView.heightAnchor.constraint(equalToConstant: 40),
            iconImageView.centerYAnchor.constraint(equalTo: self.centerYAnchor),
            
            settingLabel.topAnchor.constraint(equalTo: self.topAnchor, constant: 0),
            settingLabel.leftAnchor.constraint(equalTo: iconImageView.rightAnchor, constant: 8),
            settingLabel.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: 0),
            settingLabel.rightAnchor.constraint(equalTo: self.rightAnchor, constant: 0)
            ])
    }
    
    //MARK: UI Elements =======================================================
    
    let settingLabel:UILabel = {
        let ui = UILabel()
        ui.text = "A Setting "
       // ui.font = UIFont.systemFont(ofSize: 20)
        ui.textColor = UIColor.black
        ui.backgroundColor = UIColor.white
        return ui
    }()
    let iconImageView:UIImageView = {
        let ui = UIImageView()
        ui.image = #imageLiteral(resourceName: "aws")
        ui.contentMode = UIView.ContentMode.scaleAspectFill
        ui.clipsToBounds = true
        return ui
    }()

}

