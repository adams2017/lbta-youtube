//
//  File.swift
//  lbta-youtube
//
//  Created by Admin on 4/17/19.
//  Copyright © 2019 AX Software. All rights reserved.
//

import UIKit


class Video: Decodable {
    var thumbnailImageName:String?
    var title:String?
    var numberOfViews:Int?
    var updateDate:Date?
    var channel:Channel?
}
