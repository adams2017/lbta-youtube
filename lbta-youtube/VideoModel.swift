//
//  VideoModel.swift
//  lbta-youtube
//
//  Created by Admin on 4/13/19.
//  Copyright © 2019 AX Software. All rights reserved.
//

import Foundation

struct VideoModel {
    let artistImageView, userImageView, title, subtitle:String    
}
