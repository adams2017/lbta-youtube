//
//  MenuBar.swift
//  lbta-youtube
//
//  Created by Admin on 4/16/19.
//  Copyright © 2019 AX Software. All rights reserved.
//

/* a custom menu bar that can be layed out uner the UINavigationBar using these constraints::
 
 NSLayoutConstraint.activate([
 aMenuBar.topAnchor.constraint(equalTo: self.view.topAnchor),
 aMenuBar.leftAnchor.constraint(equalTo: self.view.leftAnchor),
 aMenuBar.rightAnchor.constraint(equalTo: self.view.rightAnchor),
 aMenuBar.heightAnchor.constraint(equalToConstant: 50)
 ])
 
 */

import UIKit

class MenuBar:UIView, UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {

    let iconNames = ["icon_home", "icon_library", "icon_chart", "icon_search" ]
    var hBarBottom, hBarLeft, hBarWidth, hBarHeight:NSLayoutConstraint!
    var homeController:HomeViewController?
    override init(frame: CGRect) {
        super.init(frame: frame )
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    func setup() {
        [menuCollectionView,horizontalBarView].forEach { $0.translatesAutoresizingMaskIntoConstraints=false; self.addSubview($0)}
        hBarBottom = horizontalBarView.bottomAnchor.constraint(equalTo: self.bottomAnchor)
        hBarLeft = horizontalBarView.leftAnchor.constraint(equalTo: self.leftAnchor)
        hBarWidth = horizontalBarView.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 1/4)
        hBarHeight = horizontalBarView.heightAnchor.constraint(equalToConstant: 8)
        
        
        //the self. anchors are created when the MenuBar is instantiated and it's constraints added
        NSLayoutConstraint.activate([
            menuCollectionView.topAnchor.constraint(equalTo: self.topAnchor, constant: 0),
            menuCollectionView.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 0),
            menuCollectionView.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: 0),
            menuCollectionView.rightAnchor.constraint(equalTo: self.rightAnchor, constant: 0),
            
            hBarBottom, hBarLeft, hBarWidth,hBarHeight
            ])
        
            menuCollectionView.register(MenuCell.self, forCellWithReuseIdentifier: MenuCell.reuseIdentifier)
            //Select a specifit item at startup and cause its color to be white
            let selectedIndexPath = IndexPath(item: 0, section: 0)
            menuCollectionView.selectItem(at: selectedIndexPath, animated: true,
                                          scrollPosition: UICollectionView.ScrollPosition.centeredHorizontally)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return iconNames.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: MenuCell.reuseIdentifier, for: indexPath) as! MenuCell
        cell.iconImageView.image = UIImage(named: iconNames[indexPath.item])?.withRenderingMode(.alwaysTemplate)
        cell.tintColor = UIColor(red: 90/255, green: 13/255, blue: 13/255, alpha: 1)
        return cell 
    }
    

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: frame.width/4, height: frame.height )
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        //one of the feed buttons was tapped
        let ix = indexPath.item
        let newLeft = self.frame.width * CGFloat(ix) / 4 // choice  0 = 0, choice 1 = 1/4 width, choice 1 is 1/2 width ,etc.
        print("frame width is ", self.frame.width)
        print("new left is", newLeft)
        hBarLeft.constant = newLeft
        //ep 12 
        homeController?.scrollToMenuIndex(menuIndex: ix) // choice 0 is cview cell 0, choice 1 is cview cell 1, etc
        
    }
    
    lazy var menuCollectionView:UICollectionView = {
        let flow = UICollectionViewFlowLayout()
        flow.minimumInteritemSpacing = 0
        flow.minimumInteritemSpacing = 0
        let ui = UICollectionView(frame: .zero, collectionViewLayout: flow)
        ui.backgroundColor = UIColor(red: 230/255, green: 30/255, blue: 30/255, alpha: 1 )
        ui.dataSource = self
        ui.delegate = self
        return ui
    }()
    
    let horizontalBarView:UIView = {
        let ui = UIView()
        ui.backgroundColor = UIColor(white: 0.9, alpha: 1)
        return ui
    }()
    
}


