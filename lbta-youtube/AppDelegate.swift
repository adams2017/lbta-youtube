//
//  AppDelegate.swift
//  lbta-youtube
//
//  Created by Admin on 4/8/19.
//  Copyright © 2019 AX Software. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?
    var imageCache =  NSCache<NSString, UIImage>()
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        window = UIWindow()
        window?.makeKeyAndVisible()
        window?.rootViewController =
            UINavigationController(rootViewController:
                HomeViewController(collectionViewLayout: UICollectionViewFlowLayout( )))
        
        UINavigationBar.appearance().barTintColor = UIColor(red: 230/255, green: 30/255, blue: 30/255, alpha: 1)
        UINavigationBar.appearance().shadowImage = UIImage()
        UINavigationBar.appearance().setBackgroundImage(UIImage(), for: .default)
        
        application.statusBarStyle = .lightContent //plist: View Controller Based Status Bar = FALSE
        guard let statusBarView = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView else {
            return true
        }
        statusBarView.backgroundColor = UIColor(red: 190/255, green: 30/255, blue: 30/255, alpha: 1)

        return true
    }
}

