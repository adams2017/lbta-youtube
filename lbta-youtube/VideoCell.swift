import UIKit

class VideoCell: UICollectionViewCell {
    //Mark: PROPERTIES
    static var reuseIdentifier: String {
        return self.description()
    }
    //MARK: LIFECYCLE
    override init(frame: CGRect) {
        super .init(frame: frame)
        setup()
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    var video:Video? {
        didSet {
            guard let video = video else {return}
            self.titleLabel.text = video.title
        
            self.artistImageView.image = UIImage(named: video.channel!.profileImageName!)
            
            let formattedDate = aDateFormatter.string(from: video.updateDate ?? Date())
            let numberExternal = aNumberFormatter.string(from: NSNumber(value: video.numberOfViews!))!
            
            self.subtitleTextView.text = video.channel!.name!
            self.subtitleTextView.text +=  " " + numberExternal + " " + formattedDate
            
            print(">>>>>",video.thumbnailImageName ?? "")
           // let videoEndpoint = URL(string: video.thumbnailImageName!)!
            
            guard let videoEndpoint = URL(string: video.thumbnailImageName!) else {
                fatalError("Can not create thumbnail image URL")
            }
            self.videoImageView.image = nil
            
            if let aImage =  imageCache.object(forKey: videoEndpoint.absoluteString as NSString) {
                self.videoImageView.image = aImage
                print("image from cache" )
            } else {
                print("image from network", videoEndpoint.absoluteString)
                let task = URLSession.shared.dataTask(with: videoEndpoint) { (data, response, error) in
                    if error != nil {
                        fatalError(error.debugDescription)
                    }
                    guard let data = data else {
                        fatalError("DATA IS NIL!")
                    }
                    let aImage = UIImage(data: data)
                  // let x =  UIApplication.shared.delegate as! AppDelegate
            
                    imageCache.setObject(aImage!,forKey: videoEndpoint.absoluteString as NSString)
                    print("from net")
                //let s = String(bytes: data!, encoding: .utf8)
                    DispatchQueue.main.async {
                        self.videoImageView.image = UIImage(data: data)
                    }
                }
                task.resume()
            }

            guard let profileEndpoint = video.channel?.profileImageName,
                  let  profileURL = URL(string: profileEndpoint) else {
                  return
            }
            self.artistImageView.image = nil
            
            if let aImage =   imageCache.object(forKey: profileEndpoint as NSString) {
                self.artistImageView.image = aImage
                print(" from cache" )
            } else {
                print("from network ")
                let task2 = URLSession.shared.dataTask(with: profileURL) { (data, response, error) in
                    if error != nil {
                        print("There is an error ", error ?? "" )
                    }
                
                    let aImage = UIImage(data: data!)
                    imageCache.setObject(aImage! , forKey: profileEndpoint as NSString)
                    //let s = String(bytes: data!, encoding: .utf8)
                    DispatchQueue.main.async {
                        self.artistImageView.image = UIImage(data: data!)
                    }
                }
                task2.resume()
                }
            }
    }
    
    /*
     artist-image-view
     
     user-image-view          title-label
     subtitle text view
     --------------------------------------------
     */
    
    func setup() {
        [videoImageView, lineSepView, artistImageView,titleLabel,subtitleTextView].forEach {
            $0.translatesAutoresizingMaskIntoConstraints = false
            self.addSubview($0)}
        
        
        NSLayoutConstraint.activate([
            videoImageView.topAnchor.constraint(equalTo: self.topAnchor, constant: 16),
            videoImageView.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 8),
            videoImageView.bottomAnchor.constraint(equalTo: artistImageView.topAnchor, constant: -4),
            videoImageView.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -8),
            
            artistImageView.bottomAnchor.constraint(equalTo: lineSepView.topAnchor, constant: -16),
            artistImageView.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 8),
            artistImageView.heightAnchor.constraint(equalToConstant: 44),
            artistImageView.widthAnchor.constraint(equalToConstant: 44),
            
            titleLabel.topAnchor.constraint(equalTo: artistImageView.topAnchor),
            titleLabel.leftAnchor.constraint(equalTo: artistImageView.rightAnchor, constant: 8),
            titleLabel.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -8),
            titleLabel.heightAnchor.constraint(equalToConstant: 20),
            
            subtitleTextView.bottomAnchor.constraint(equalTo: artistImageView.bottomAnchor, constant: 4),
            subtitleTextView.leftAnchor.constraint(equalTo: artistImageView.rightAnchor, constant: 8),
            subtitleTextView.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -8),
            subtitleTextView.heightAnchor.constraint(equalToConstant: 30),
            
            
            lineSepView.heightAnchor.constraint(equalToConstant: 1),
            lineSepView.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 0),
            lineSepView.rightAnchor.constraint(equalTo: self.rightAnchor, constant: 0),
            lineSepView.bottomAnchor.constraint(equalTo: self.bottomAnchor)
            ])
    
    }
    //MARK: UI Elements =======================================================
    
    let titleLabel:UILabel = {
        let ui = UILabel()
        ui.text =  "Taylor Swift Blank Space"
        ui.backgroundColor = UIColor.white
        return ui
    }()
    
    let videoImageView:UIImageView = {
        let ui = UIImageView()
        ui.image = #imageLiteral(resourceName: "book_club_splash")
        ui.backgroundColor = UIColor.blue
        ui.contentMode = UIView.ContentMode.scaleAspectFill
        ui.clipsToBounds = true
        return ui
    }()
    let lineSepView:UIView = {
        let ui = UIView()
        ui.backgroundColor = UIColor.black
        return ui
    }()
    let artistImageView:UIImageView = {
        let ui = UIImageView()
       // ui.backgroundColor = .yellow
        ui.image = #imageLiteral(resourceName: "steve_jobs")
        ui.contentMode = UIView.ContentMode.scaleAspectFill
        ui.clipsToBounds = true
        ui.layer.cornerRadius = 44/2
        return ui
    }()
    let subtitleTextView:UITextView = {
        let ui = UITextView()
        ui.text = "Taylor Swift Views: • 3,210,000 -  Since over 10 year ago !!!"
        ui.isEditable = false
        ui.textContainerInset = UIEdgeInsets(top: 2.5, left: -2, bottom: 0, right: 0)
        ui.backgroundColor = UIColor.yellow
        ui.textColor = UIColor.lightGray
        return ui
    }()
    
    let aDateFormatter:DateFormatter = {
        let df = DateFormatter()
        df.dateFormat = "M/d/y"
        return df
    }()
    
    let aNumberFormatter:NumberFormatter = {
        let nf = NumberFormatter()
        nf.numberStyle = .decimal
        return nf
    }()

}
//=========
let imageCache = NSCache<NSString, UIImage>()
//=========
